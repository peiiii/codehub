import os
pkg_root_dir=os.path.dirname(__file__)
pkg_data_dir=os.path.join(pkg_root_dir,'data')
code_dir=os.path.join(pkg_root_dir,'code')

if not os.path.exists(code_dir):
    os.makedirs(code_dir)